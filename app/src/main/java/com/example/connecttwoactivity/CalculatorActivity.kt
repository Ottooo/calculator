package com.example.connecttwoactivity

import android.os.Bundle
import android.view.View
import android.view.View.OnLongClickListener
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_calculator.*


class CalculatorActivity : AppCompatActivity(), View.OnClickListener {
    private var variableFirst: Double = 0.0
    private var variableSecond: Double = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)
        init()

    }

    private fun init() {
        Button0.setOnClickListener(this)
        Button1.setOnClickListener(this)
        Button2.setOnClickListener(this)
        Button3.setOnClickListener(this)
        Button4.setOnClickListener(this)
        Button5.setOnClickListener(this)
        Button6.setOnClickListener(this)
        Button7.setOnClickListener(this)
        Button8.setOnClickListener(this)
        Button9.setOnClickListener(this)
        dotButton.setOnClickListener() {
            if (resultTextView.text.toString().isNotEmpty()) {
                resultTextView.text = resultTextView.text.toString() + "."
                dotButton.isClickable = false
                if (resultTextView.text.toString().isEmpty()) {
                    dotButton.isClickable = true
                }
            }
        }
        backspaceButton.setOnLongClickListener {
            if (resultTextView.text.isNotEmpty())
                resultTextView.text=""
            true
        }
    }


    private fun variableOperation() {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            variableFirst = resultTextView.text.toString().toDouble()
            resultTextView.text = ""
            dotButton.isClickable = true

        }
    }

    private fun operationResult() {
        var result: Double = 0.0
        if (operation==":" && variableSecond==0.0){
            Toast.makeText(this,"Zero division error",Toast.LENGTH_LONG).show()
        } else if (operation == ":" && variableFirst != 0.0) {
            result = variableFirst / variableSecond
        }  else if (operation == "+") {
            result = variableFirst + variableSecond
        } else if (operation == "*") {
            result = variableFirst * variableSecond
        } else if (operation == "-") {
            result = variableFirst - variableSecond
        }
        resultTextView.text = ""
        resultTextView.text = resultTextView.text.toString() + result
    }

    fun equal(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            variableSecond = value.toDouble()
            operationResult()
        }
    }

    fun substract(view: View) {
        variableOperation()
        operation = "-"
    }

    fun plus(view: View) {
        variableOperation()
        operation = "+"
    }

    fun divide(view: View) {
        variableOperation()
        operation = ":"
    }

    fun multiply(view: View) {
        variableOperation()
        operation = "*"
    }

    fun backSpace(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            resultTextView.text = value.substring(0, value.length - 1)
            dotButton.isClickable = "." !in resultTextView.text.toString()
        }

    }

    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text = resultTextView.text.toString() + button.text.toString()
    }
}
